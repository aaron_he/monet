import {Menu} from "@/module/Menu";


export interface MenuResponse {
  ads: string[];
  menus: Menu[];
}
