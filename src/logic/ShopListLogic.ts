import {ApiHelper} from "@/api/ApiHelper";
import {Shop} from "@/module/Shop";
import {City} from "@/module/City";
import {GlobalSetting} from "@/GlobalSetting";

export class ShopListLogic {

	private apiHelper = ApiHelper.getInstance();
	private globalSetting: GlobalSetting = GlobalSetting.getInstance();
	
	private static shopListLogic: ShopListLogic;
	private shopList:Shop[] = [];
	private data:Shop[] =[];
	private cities!:City[];
	private currentCity: string = "";
	public currentCityId: number = 0;
	// private user_latlng:{lat:number,lng:number} = {lat:0,lng:0}

	public static getInstance() {
		if (!this.shopListLogic) {
		this.shopListLogic = new ShopListLogic();
		}
		return this.shopListLogic;
	}
	
	public async fetchCityList(){
		const response:any = await this.apiHelper.getCityList()
		this.cities = response
	}
	
	public getCityList(){
		return this.cities;
	}
	
	public fetchUserLocation(){
		return new Promise((resolve, reject) => {
			uni.getLocation({
				type: 'wgs84',
				altitude: true,
				success: res => {
					// console.log(res)
					// @ts-ignore
					this.globalSetting.setUserLatLng(res.latitude, res.longitude)
					// @ts-ignore
					this.globalSetting.setUserLatLngStorage(res.latitude, res.longitude)
					resolve(res) //then接收
				},
				fail: res=>{
					uni.showToast({
						icon: 'none',
						title: '您取消了位置授权，无法获取您附近的优惠订单。'
					});
					reject(res)
				}
			})
		})
	}
	
	// 获取用户所在城市
	public async fetchCustomerCity(lat: number, lng: number){
		const response:any = await this.apiHelper.getCustomerCity(lat + "," + lng)
		this.currentCity = response.city_id===0 ? "请点击选择城市" : response.memo
		this.currentCityId = response.city_id
		this.globalSetting.setCity_id(Number(this.currentCityId))
	}
	
	public getCustomerCity(){
		return this.currentCity;
	}
	
	public getCustomerCityId(){
		return this.currentCityId;
	}
	
	public async fetchShopList(currentCityId: number){
		let user_latlng = this.globalSetting.getUserLatLng()
		const response:any = await this.apiHelper.getShopListV2(currentCityId, user_latlng.lat, user_latlng.lng)
		this.shopList = response.list
	}
	
	public getShopList(){
		return this.shopList
	}
	
	
	
}