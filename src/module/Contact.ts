export interface Contact {
  name: string,
  tel:string,
  addr:string,
  unit: string,
  buzz_code: string,
}

export class Contact {
  name = ""
  tel = ""
  addr = ""
  unit = ""
  buzz_code = ""
}
