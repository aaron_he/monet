import {ApiHelper} from "@/api/ApiHelper";
	import {GlobalSetting} from "@/GlobalSetting";
import {CartLogic} from "@/logic/CartLogic";
import {CouponValidation, CouponValidationProduct, ValidationResult, CouponListElement} from "@/module/Coupon";

export class CouponLogic {
  private static couponLogic: CouponLogic;
  private globalSetting: GlobalSetting = GlobalSetting.getInstance();
  private apiHelper = ApiHelper.getInstance();
  private cartLogic: CartLogic = CartLogic.getInstance();
  private couponList: CouponListElement[] = [];

  public static getInstance() {
    if (!this.couponLogic) {
      this.couponLogic = new CouponLogic();
    }
    return this.couponLogic;
  }

  public async fetchCouponList(codeEntered: string) {
	  let params = {
	  	used: 0,
	  	shop_id: this.cartLogic.getShopId(),
	  	shipping_type: this.cartLogic.getDeliveryType(),
	  	promo_code: codeEntered
	  }
    const response: any = await this.apiHelper.fetchCouponList(params);
	this.couponList = response
	return response
  }

  public getCouponList() {
    return this.couponList;
  }
  
  
  public async verify_coupon(couponCodes: string) {
	  let params = {
	  	auto_match: 0,
	  	shop_id: this.cartLogic.getShopId(),
	  	shipping_type: this.cartLogic.getDeliveryType(),
	  	promo_codes: couponCodes,
	  	subtotal: this.cartLogic.getSubTotal(),
	  	product_items: JSON.stringify(this.cartLogic.getCouponValidationProducts())
	  }
    const response: any = await this.apiHelper.verify_coupon(params);
  	return response
  }
  
  public async checkPromo(codeEntered: string) {
	  let params = {
	  	shop_id: this.cartLogic.getShopId(),
	  	code: codeEntered,
	  	code_type: 'coupon_code',
	  	customer_id: this.globalSetting.getUser().cid
	  }
    const response: any = await this.apiHelper.checkPromo(params);
  	return response
  }
  
  public async getPromocodeList(params: object) {
    const response: any = await this.apiHelper.getPromocodeList(params);
  	return response
  }
  
  getCouponStr(coupon: CouponValidation): string {
      const paramsArr = [];
      for (const key in coupon) {
        paramsArr.push(key + '=' + coupon[key].toString())
      }
      return paramsArr.join('&');
    }

  // private checkResState(res: any): boolean {
  //   if (res.data.status === 0) {
  //     alert(res.data.error);
  //     return false;
  //   } else {
  //     return true;
  //   }
  // }
}
